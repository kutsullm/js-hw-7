const arr = [1,2,3,4,"str",true,"hello"]
const type = "string"

function filterBy(arr,type){
    return arr.filter((item)=>{
        return typeof(item) !== type
    })
}
console.log(filterBy(arr,type))